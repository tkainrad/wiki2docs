# Welcome to Wiki2Docs

This documentation site illustrates how to setup a static site with GitLab pages from a GitLab project's Wiki pages.

The content from this site itself is generated from the Wiki pages of a GitLab project.  You can browse its source code and Wiki at [https://gitlab.com/tkainrad/wiki2docs](https://gitlab.com/tkainrad/wiki2docs). The underlying GitLab Wiki page for this main page is located at [https://gitlab.com/tkainrad/wiki2docs/wikis/docs/index](https://gitlab.com/tkainrad/wiki2docs/wikis/docs/index).

## How it works

In general, the CI/CD configuration for GitLab Pages can only make use of the files in a project's main repository. It can not use the files of the Wiki repository directly.

Therefore, we use [GitLab's repository mirroring feature](https://docs.gitlab.com/ee/workflow/repository_mirroring.html#pulling-from-a-remote-repository-starter). This way, a main GitLab repository can monitor its own Wiki repository and subsequently use the markdown files for static site generation.