# Adding content to the generated GitLab Pages Documentation Site

In order to edit your online documentation, simply edit the Wiki files in the docs subfolder of this Wiki. You can use GitLab's online interface. 

Once you commit changes, [GitLab's repository mirroring feature](https://docs.gitlab.com/ee/workflow/repository_mirroring.html#pulling-from-a-remote-repository-starter) will eventually propagate these changes to the project repository connected to the static site. This will usually take around one minute. Your changes will then be live on the static docs site, in the case of this project, the site is [https://tkainrad.gitlab.io/wiki2docs](https://tkainrad.gitlab.io/wiki2docs).